# SimplePot

## Getting started

### Build from sources

1. Build project:
 > cargo build --release

2. Run with default config:
 > ./target/release/simple_pot config/default.yml

3. Check HoneyPot:
 > curl http://127.0.0.1:3030

And response:
 > I'm SimplePot
