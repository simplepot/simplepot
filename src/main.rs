extern crate yaml_rust;
use yaml_rust::YamlLoader;
use warp::Filter;
use std::net::Ipv4Addr;
use std::str::FromStr;
use std::env;
use std::fs::File;
use std::io::prelude::*;


#[tokio::main]
 async fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() > 1 {
        println!("The first argument is {}", args[1]);
    }
    // ToDo: implement named argument
    let config_path = if args.len() > 1  { &args[1] } else { "default.yml" };
    let mut file = File::open(config_path).expect("Unable to open file");
    let mut contents = String::new();

    file.read_to_string(&mut contents).expect("Unable to read file");
    let configs = YamlLoader::load_from_str(&contents).unwrap();

    // Multi document support, doc is a yaml::Yaml
    let config = &configs[0];

    // Debug support
    println!("{:?}", config);

    let routes = warp::any().map(|| "I'm SimplePot");
    // ToDo: implement hostname in config
    let ip = Ipv4Addr::from_str(config["ip"].as_str().unwrap()).unwrap();
    let port = u16::try_from(config["port"].as_i64().unwrap()).unwrap();
    println!("{:?}", ip.octets());
    warp::serve(routes).run((ip.octets(), port)).await;
}
